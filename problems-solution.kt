

fun main() {
    println("Prueba1: " + prueba1(3, 10, arrayOf(1, 2, 8), arrayOf(4, 5, 9)))
    println("Prueba2: " + prueba2(arrayOf(1, 3, 2, 5, 4)))
    println("Prueba3: " + prueba3(-5, -5, 5, 5))
}

fun prueba1 (n: Int, m: Int, A: Array<Int>, B: Array<Int>) : Int {
    val availableDays: MutableSet<Int> = mutableSetOf()
    for (num in 0..n-1) {
        availableDays.addAll(A[num]..B[num])
    }
    
    return availableDays.size
}

fun prueba2 (a: Array<Int>) : Int {
    var pairs : Int = 0
    for (i in 0..a.size-2) {
        val current = a[i]
        val temp = a.copyOfRange(i+1, a.size)
        val currentPairs = temp.filter { j -> j < current }.size
        pairs += currentPairs
    }
    
    return pairs
}

fun prueba3 (a: Int, b: Int, c:Int, d: Int): Int {
    var x2 = c
    var Y2 = d
    var dirX = if (a < c) 'r' else 'l' // r: right, l: left
    var dirY = if (b < d) 'u' else 'd' // u: up, d: down
    var auxX = a
    var auxY = b
    var movType = 0 // 0 or 1, mov type
    var movs = 0 // mov counts
    var doMov = false
    
    while (auxX != x2 || auxY != Y2) {
        doMov = false
        if (movType == 0) {
            if (((dirX == 'r' && auxX + 2 <= x2) || (dirX == 'l' && auxX - 2 >= x2)) && (auxY - Y2) != 0) {
                auxX = if (dirX == 'r') auxX + 2 else auxX - 2
                auxY = if (dirY == 'u') auxY + 1 else auxY - 1
                movs++
                doMov = true
            }
            movType = 1
        } 
        
        if (movType == 1 && doMov == false) {
            if (((dirY == 'u' && auxY + 2 <= Y2) || (dirY == 'd' && auxY - 2 >= Y2)) && (auxX - x2) != 0) {
                auxY = if (dirY == 'u') auxY + 2 else auxY - 2
                auxX = if (dirX == 'r') auxX + 1 else auxX - 1
                movs++
                doMov = true
            }
            movType = 0
        }
        
        if (doMov == false) {
            x2 = Math.abs(x2)
            Y2 = Math.abs(Y2)
            auxX = Math.abs(auxX)
            auxY = Math.abs(auxY)
            dirX = if (auxX < x2) 'r' else 'l'
            dirY = if (auxY < Y2) 'u' else 'd'
            if ( Math.abs(x2 - auxX) == 1 && Math.abs(Y2 - auxY) == 1) {
                movs = movs + 2
                return movs
            } else if (((x2 - auxX == 0) && Math.abs(Y2 - auxY) == 1) ||  ((Y2 - auxY == 0) && Math.abs(x2 - auxX) == 1)) {
                movs = movs + 3
                return movs
            } else {
                if (Math.abs(x2 - auxX) == 0 || Math.abs(Y2 - auxY) == 0) {
                    if (Math.abs(x2 - auxX) == 0) {
                        if (auxX > x2) {
                            auxX = auxX - (auxX - x2)*2
                        }
                        auxX = auxX - 2
                        auxY = auxY + 1
                        movs++
                    } else {
                        if (auxY > Y2) {
                            auxY = auxY - (auxY - Y2)*2
                        }
                        auxY = auxY - 2
                        auxX = auxX + 1
                        movs++
                    }
                    dirX = if (auxX < x2) 'r' else 'l'
                    dirY = if (auxY < Y2) 'u' else 'd'
                }
            }
        }
    }
    
    return movs
}
